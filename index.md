# Home
Conceptos básicos sobre Big Data con Pyspark

## Material

El material está disponible en el siguiente [repositorio](https://gitlab.com/FAAM/python_big_data), para obtener el código de fuente basta con que ejecutes el siguiente comando:

```
https://gitlab.com/FAAM/python_big_data
```

## Contenidos

```{tableofcontents}
```