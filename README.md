# Conceptos básicos sobre Big Data con Pyspark

<a href="https://fralfaro.gitlab.io/python_big_data/"><img alt="Link a la Documentación" src="https://img.shields.io/badge/jupyter--book-link-brightgreen"></a>
[![pipeline status](https://gitlab.com/FAAM/python_big_data/badges/master/pipeline.svg)](https://gitlab.com/FAAM/python_big_data/-/commits/master)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/FAAM%2Fpython_big_data/HEAD)


## Contenidos temáticos
- Concurrencia y paralelismo
- Data Lake, parquet, delta, etc.
- MapReduce
- Introducción a Pyspark
- Introducción a Koalas





