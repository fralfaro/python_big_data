# Pyspark

## Introducción

[PySpark](https://spark.apache.org/docs/latest/api/python/index.html) es una interfaz para **Apache Spark** en Python. No solo le permite escribir aplicaciones Spark utilizando las API de Python, sino que también proporciona el shell de PySpark para analizar interactivamente sus datos en un entorno distribuido. PySpark es compatible con la mayoría de las funciones de Spark, como Spark SQL, DataFrame, Streaming, MLlib (aprendizaje automático) y Spark Core.

<img src="images/pyspark_001.png" alt="" align="center"/>

### Spark SQL y DataFrame

Spark SQL es un módulo de Spark para el procesamiento de datos estructurados. Proporciona una abstracción de programación llamada DataFrame y también puede actuar como motor de consultas SQL distribuido.

### Streaming

Ejecutando sobre Spark, la función de transmisión en Apache Spark permite poderosas aplicaciones interactivas y analíticas tanto en transmisión como en datos históricos, al tiempo que hereda las características de tolerancia a fallas y facilidad de uso de Spark.

### MLlib

Construido sobre Spark, MLlib es una biblioteca de aprendizaje automático escalable que proporciona un conjunto uniforme de API de alto nivel que ayudan a los usuarios a crear y ajustar canales prácticos de aprendizaje automático.

### Spark Core

Spark Core es el motor de ejecución general subyacente para la plataforma Spark sobre el que se construyen todas las demás funciones. Proporciona un RDD (Resilient Distributed Dataset) y capacidades de computación en memoria.

## Quick Start

Esta es una breve introducción y guía de inicio rápido de la API PySpark DataFrame. Los DataFrames de PySpark se evalúan de forma perezosa. Se implementan sobre los [RDD](https://spark.apache.org/docs/latest/rdd-programming-guide.html#overview). Cuando Spark transforma los datos, no calcula inmediatamente la transformación, sino que planifica cómo calcularla más adelante. Cuando se llaman explícitamente acciones como `collect()`, comienza el cálculo. Este cuaderno muestra los usos básicos del DataFrame, dirigido principalmente a nuevos usuarios. Puede ejecutar la última versión de estos ejemplos usted mismo en un cuaderno en vivo aquí.

También hay otra información útil en el sitio de documentación de Apache Spark, consulte la última versión de [Spark SQL and DataFrames](https://spark.apache.org/docs/latest/sql-programming-guide.html), [RDD Programming Guide](https://spark.apache.org/docs/latest/rdd-programming-guide.html), [Structured Streaming Programming Guide](https://spark.apache.org/docs/latest/structured-streaming-programming-guide.html), [Spark Streaming Programming Guide](https://spark.apache.org/docs/latest/streaming-programming-guide.html) and [Machine Learning Library (MLlib) Guide](https://spark.apache.org/docs/latest/ml-guide.html).


Las aplicaciones de PySpark comienzan con la inicialización de `SparkSession`, que es el punto de entrada de PySpark como se muestra a continuación. En caso de ejecutarlo en el shell PySpark a través del ejecutable pyspark, el shell crea automáticamente la sesión en la variable spark para los usuarios.


```python
from pyspark.sql import SparkSession

spark = SparkSession.builder.getOrCreate()
```

### Creación de DataFrame
Un PySpark DataFrame se puede crear a través de `pyspark.sql.SparkSession.createDataFrame` típicamente pasando una lista de listas, tuplas, diccionarios y `pyspark.sql.Rows`, un pandas DataFrame y un RDD que consta de dicha lista. `pyspark.sql.SparkSession.createDataFrame` toma el argumento del `schema` para especificar el esquema del DataFrame. Cuando se omite, PySpark infiere el esquema correspondiente tomando una muestra de los datos.

En primer lugar, puede crear un DataFrame de PySpark a partir de una lista de filas


```python
from datetime import datetime, date
import pandas as pd
from pyspark.sql import Row

df = spark.createDataFrame([
    Row(a=1, b=2., c='string1', d=date(2000, 1, 1), e=datetime(2000, 1, 1, 12, 0)),
    Row(a=2, b=3., c='string2', d=date(2000, 2, 1), e=datetime(2000, 1, 2, 12, 0)),
    Row(a=4, b=5., c='string3', d=date(2000, 3, 1), e=datetime(2000, 1, 3, 12, 0))
])
df
```




    DataFrame[a: bigint, b: double, c: string, d: date, e: timestamp]



Cree un DataFrame de PySpark con un esquema explícito.


```python
df = spark.createDataFrame([
    (1, 2., 'string1', date(2000, 1, 1), datetime(2000, 1, 1, 12, 0)),
    (2, 3., 'string2', date(2000, 2, 1), datetime(2000, 1, 2, 12, 0)),
    (3, 4., 'string3', date(2000, 3, 1), datetime(2000, 1, 3, 12, 0))
], schema='a long, b double, c string, d date, e timestamp')
df
```




    DataFrame[a: bigint, b: double, c: string, d: date, e: timestamp]



Cree un DataFrame de PySpark a partir de un DataFrame de pandas


```python
pandas_df = pd.DataFrame({
    'a': [1, 2, 3],
    'b': [2., 3., 4.],
    'c': ['string1', 'string2', 'string3'],
    'd': [date(2000, 1, 1), date(2000, 2, 1), date(2000, 3, 1)],
    'e': [datetime(2000, 1, 1, 12, 0), datetime(2000, 1, 2, 12, 0), datetime(2000, 1, 3, 12, 0)]
})
df = spark.createDataFrame(pandas_df)
df
```




    DataFrame[a: bigint, b: double, c: string, d: date, e: timestamp]



Cree un DataFrame de PySpark a partir de un RDD que consta de una lista de tuplas.


```python
rdd = spark.sparkContext.parallelize([
    (1, 2., 'string1', date(2000, 1, 1), datetime(2000, 1, 1, 12, 0)),
    (2, 3., 'string2', date(2000, 2, 1), datetime(2000, 1, 2, 12, 0)),
    (3, 4., 'string3', date(2000, 3, 1), datetime(2000, 1, 3, 12, 0))
])
df = spark.createDataFrame(rdd, schema=['a', 'b', 'c', 'd', 'e'])
df
```




    DataFrame[a: bigint, b: double, c: string, d: date, e: timestamp]



Los DataFrames creados anteriormente tienen los mismos resultados y esquema.


```python
# All DataFrames above result same.
df.show()
df.printSchema()
```

    +---+---+-------+----------+-------------------+
    |  a|  b|      c|         d|                  e|
    +---+---+-------+----------+-------------------+
    |  1|2.0|string1|2000-01-01|2000-01-01 12:00:00|
    |  2|3.0|string2|2000-02-01|2000-01-02 12:00:00|
    |  3|4.0|string3|2000-03-01|2000-01-03 12:00:00|
    +---+---+-------+----------+-------------------+
    
    root
     |-- a: long (nullable = true)
     |-- b: double (nullable = true)
     |-- c: string (nullable = true)
     |-- d: date (nullable = true)
     |-- e: timestamp (nullable = true)
    


### Ver los datos

Las filas superiores de un DataFrame se pueden mostrar usando `DataFrame.show()`.


```python
df.show(1)
```

    +---+---+-------+----------+-------------------+
    |  a|  b|      c|         d|                  e|
    +---+---+-------+----------+-------------------+
    |  1|2.0|string1|2000-01-01|2000-01-01 12:00:00|
    +---+---+-------+----------+-------------------+
    only showing top 1 row
    


Alternativamente, puede habilitar la configuración `spark.sql.repl.eagerEval.enabled` para la evaluación ansiosa de PySpark DataFrame en notebooks como Jupyter. El número de filas para mostrar se puede controlar mediante la configuración de `spark.sql.repl.eagerEval.maxNumRows`.


```python
spark.conf.set('spark.sql.repl.eagerEval.enabled', True)
df
```




<table border='1'>
<tr><th>a</th><th>b</th><th>c</th><th>d</th><th>e</th></tr>
<tr><td>1</td><td>2.0</td><td>string1</td><td>2000-01-01</td><td>2000-01-01 12:00:00</td></tr>
<tr><td>2</td><td>3.0</td><td>string2</td><td>2000-02-01</td><td>2000-01-02 12:00:00</td></tr>
<tr><td>3</td><td>4.0</td><td>string3</td><td>2000-03-01</td><td>2000-01-03 12:00:00</td></tr>
</table>




Las filas también se pueden mostrar verticalmente. Esto es útil cuando las filas son demasiado largas para mostrarse horizontalmente.


```python
df.show(1, vertical=True)
```

    -RECORD 0------------------
     a   | 1                   
     b   | 2.0                 
     c   | string1             
     d   | 2000-01-01          
     e   | 2000-01-01 12:00:00 
    only showing top 1 row
    


Puede ver el esquema del DataFrame y los nombres de las columnas de la siguiente manera:



```python
df.columns
```




    ['a', 'b', 'c', 'd', 'e']




```python
df.printSchema()
```

    root
     |-- a: long (nullable = true)
     |-- b: double (nullable = true)
     |-- c: string (nullable = true)
     |-- d: date (nullable = true)
     |-- e: timestamp (nullable = true)
    


Mostrar el resumen del DataFrame


```python
df.select("a", "b", "c").describe().show()
```

    +-------+---+---+-------+
    |summary|  a|  b|      c|
    +-------+---+---+-------+
    |  count|  3|  3|      3|
    |   mean|2.0|3.0|   null|
    | stddev|1.0|1.0|   null|
    |    min|  1|2.0|string1|
    |    max|  3|4.0|string3|
    +-------+---+---+-------+
    


`DataFrame.collect()` recopila los datos distribuidos al lado del controlador como los datos locales en Python. Tenga en cuenta que esto puede generar un error de memoria insuficiente cuando el conjunto de datos es demasiado grande para caber en el lado del controlador porque recopila todos los datos de los ejecutores al lado del controlador.


```python
df.collect()
```




    [Row(a=1, b=2.0, c='string1', d=datetime.date(2000, 1, 1), e=datetime.datetime(2000, 1, 1, 12, 0)),
     Row(a=2, b=3.0, c='string2', d=datetime.date(2000, 2, 1), e=datetime.datetime(2000, 1, 2, 12, 0)),
     Row(a=3, b=4.0, c='string3', d=datetime.date(2000, 3, 1), e=datetime.datetime(2000, 1, 3, 12, 0))]



Para evitar lanzar una excepción de memoria insuficiente, use `DataFrame.take()` o `DataFrame.tail()`.


```python
df.take(1)
```




    [Row(a=1, b=2.0, c='string1', d=datetime.date(2000, 1, 1), e=datetime.datetime(2000, 1, 1, 12, 0))]



PySpark DataFrame también proporciona la conversión de nuevo a pandas DataFrame para aprovechar las API de pandas. Tenga en cuenta que `toPandas` también recopila todos los datos en el lado del conductor que pueden causar fácilmente un error de falta de memoria cuando los datos son demasiado grandes para caber en el lado del conductor.


```python
df.toPandas()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>a</th>
      <th>b</th>
      <th>c</th>
      <th>d</th>
      <th>e</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>2.0</td>
      <td>string1</td>
      <td>2000-01-01</td>
      <td>2000-01-01 12:00:00</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>3.0</td>
      <td>string2</td>
      <td>2000-02-01</td>
      <td>2000-01-02 12:00:00</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>4.0</td>
      <td>string3</td>
      <td>2000-03-01</td>
      <td>2000-01-03 12:00:00</td>
    </tr>
  </tbody>
</table>
</div>



### Seleccionar datos y acceder a ellos
PySpark DataFrame se evalúa de manera perezosa y simplemente seleccionar una columna no activa el cálculo, pero devuelve una instancia de `Column`.


```python
df.a
```




    Column<b'a'>



De hecho, la mayoría de las operaciones de columna devuelven instancias `Column`.


```python
from pyspark.sql import Column
from pyspark.sql.functions import upper

type(df.c) == type(upper(df.c)) == type(df.c.isNull())
```




    True



Estas columnas se pueden utilizar para seleccionar las columnas de un DataFrame. Por ejemplo, `DataFrame.select()` toma las instancias de `Column` que devuelven otro DataFrame.


```python
df.select(df.c).show()
```

    +-------+
    |      c|
    +-------+
    |string1|
    |string2|
    |string3|
    +-------+
    


Asignar nueva instancia de `Column`.


```python
df.withColumn('upper_c', upper(df.c)).show()
```

    +---+---+-------+----------+-------------------+-------+
    |  a|  b|      c|         d|                  e|upper_c|
    +---+---+-------+----------+-------------------+-------+
    |  1|2.0|string1|2000-01-01|2000-01-01 12:00:00|STRING1|
    |  2|3.0|string2|2000-02-01|2000-01-02 12:00:00|STRING2|
    |  3|4.0|string3|2000-03-01|2000-01-03 12:00:00|STRING3|
    +---+---+-------+----------+-------------------+-------+
    


Para seleccionar un subconjunto de filas, use `DataFrame.filter()`.


```python
df.filter(df.a == 1).show()
```

    +---+---+-------+----------+-------------------+
    |  a|  b|      c|         d|                  e|
    +---+---+-------+----------+-------------------+
    |  1|2.0|string1|2000-01-01|2000-01-01 12:00:00|
    +---+---+-------+----------+-------------------+
    


### Aplicar una función
PySpark admite varias UDF y API para permitir a los usuarios ejecutar funciones nativas de Python. Consulte [UDF de Pandas](https://spark.apache.org/docs/latest/sql-pyspark-pandas-with-arrow.html#pandas-udfs-aka-vectorized-udfs) y [API de funciones de Pandas](https://spark.apache.org/docs/latest/sql-pyspark-pandas-with-arrow.html#pandas-function-apis). Por ejemplo, el siguiente ejemplo permite a los usuarios utilizar directamente las API en una serie pandas dentro de la función nativa de Python.


```python
import pandas
from pyspark.sql.functions import pandas_udf

@pandas_udf('long')
def pandas_plus_one(series: pd.Series) -> pd.Series:
    # Simply plus one by using pandas Series.
    return series + 1

df.select(pandas_plus_one(df.a)).show()
```

    +------------------+
    |pandas_plus_one(a)|
    +------------------+
    |                 2|
    |                 3|
    |                 4|
    +------------------+
    


Otro ejemplo es `DataFrame.mapInPandas`, que permite a los usuarios usar directamente las API en un DataFrame de pandas sin restricciones como la longitud del resultado.


```python
def pandas_filter_func(iterator):
    for pandas_df in iterator:
        yield pandas_df[pandas_df.a == 1]

df.mapInPandas(pandas_filter_func, schema=df.schema).show()
```

    +---+---+-------+----------+-------------------+
    |  a|  b|      c|         d|                  e|
    +---+---+-------+----------+-------------------+
    |  1|2.0|string1|2000-01-01|2000-01-01 12:00:00|
    +---+---+-------+----------+-------------------+
    


### Agrupar datos

PySpark DataFrame también proporciona una forma de manejar datos agrupados mediante el enfoque común, la estrategia dividir-aplicar-combinar. Agrupa los datos según una determinada condición, aplica una función a cada grupo y luego los vuelve a combinar con el DataFrame.


```python
df = spark.createDataFrame([
    ['red', 'banana', 1, 10], ['blue', 'banana', 2, 20], ['red', 'carrot', 3, 30],
    ['blue', 'grape', 4, 40], ['red', 'carrot', 5, 50], ['black', 'carrot', 6, 60],
    ['red', 'banana', 7, 70], ['red', 'grape', 8, 80]], schema=['color', 'fruit', 'v1', 'v2'])
df.show()
```

    +-----+------+---+---+
    |color| fruit| v1| v2|
    +-----+------+---+---+
    |  red|banana|  1| 10|
    | blue|banana|  2| 20|
    |  red|carrot|  3| 30|
    | blue| grape|  4| 40|
    |  red|carrot|  5| 50|
    |black|carrot|  6| 60|
    |  red|banana|  7| 70|
    |  red| grape|  8| 80|
    +-----+------+---+---+
    


Agrupar y luego aplicar la función `avg()` a los grupos resultantes.


```python
df.groupby('color').avg().show()

```

    +-----+-------+-------+
    |color|avg(v1)|avg(v2)|
    +-----+-------+-------+
    |  red|    4.8|   48.0|
    |black|    6.0|   60.0|
    | blue|    3.0|   30.0|
    +-----+-------+-------+
    


También puede aplicar una función nativa de Python contra cada grupo mediante el uso de las API de pandas.


```python
def plus_mean(pandas_df):
    return pandas_df.assign(v1=pandas_df.v1 - pandas_df.v1.mean())

df.groupby('color').applyInPandas(plus_mean, schema=df.schema).show()
```

    +-----+------+---+---+
    |color| fruit| v1| v2|
    +-----+------+---+---+
    |  red|banana| -3| 10|
    |  red|carrot| -1| 30|
    |  red|carrot|  0| 50|
    |  red|banana|  2| 70|
    |  red| grape|  3| 80|
    |black|carrot|  0| 60|
    | blue|banana| -1| 20|
    | blue| grape|  1| 40|
    +-----+------+---+---+
    


Coagrupar y aplicar una función.


```python
df1 = spark.createDataFrame(
    [(20000101, 1, 1.0), (20000101, 2, 2.0), (20000102, 1, 3.0), (20000102, 2, 4.0)],
    ('time', 'id', 'v1'))

df2 = spark.createDataFrame(
    [(20000101, 1, 'x'), (20000101, 2, 'y')],
    ('time', 'id', 'v2'))

def asof_join(l, r):
    return pd.merge_asof(l, r, on='time', by='id')

df1.groupby('id').cogroup(df2.groupby('id')).applyInPandas(
    asof_join, schema='time int, id int, v1 double, v2 string').show()
```

    +--------+---+---+---+
    |    time| id| v1| v2|
    +--------+---+---+---+
    |20000101|  1|1.0|  x|
    |20000102|  1|3.0|  x|
    |20000101|  2|2.0|  y|
    |20000102|  2|4.0|  y|
    +--------+---+---+---+
    


### Data in/out
CSV es sencillo y fácil de usar. Parquet y ORC son formatos de archivo compactos y eficientes para leer y escribir más rápido.

Hay muchas otras fuentes de datos disponibles en PySpark, como JDBC, text, binaryFile, Avro, etc. Consulte también la última [Guía de Spark SQL, DataFrames y Datasets](https://spark.apache.org/docs/latest/sql-programming-guide.html) en la documentación de Apache Spark.

#### CSV



```python
df.write.csv('foo.csv', header=True)
spark.read.csv('foo.csv', header=True).show()
```

    +-----+------+---+---+
    |color| fruit| v1| v2|
    +-----+------+---+---+
    |black|carrot|  6| 60|
    | blue|banana|  2| 20|
    |  red|banana|  1| 10|
    | blue| grape|  4| 40|
    |  red|carrot|  3| 30|
    |  red|carrot|  5| 50|
    |  red|banana|  7| 70|
    |  red| grape|  8| 80|
    +-----+------+---+---+
    


#### Parquet



```python
df.write.parquet('bar.parquet')
spark.read.parquet('bar.parquet').show()
```

    +-----+------+---+---+
    |color| fruit| v1| v2|
    +-----+------+---+---+
    |black|carrot|  6| 60|
    | blue|banana|  2| 20|
    | blue| grape|  4| 40|
    |  red|banana|  1| 10|
    |  red|carrot|  5| 50|
    |  red|banana|  7| 70|
    |  red|carrot|  3| 30|
    |  red| grape|  8| 80|
    +-----+------+---+---+
    


#### ORC


```python
df.write.orc('zoo.orc')
spark.read.orc('zoo.orc').show()
```

    +-----+------+---+---+
    |color| fruit| v1| v2|
    +-----+------+---+---+
    |  red|banana|  7| 70|
    |  red| grape|  8| 80|
    |black|carrot|  6| 60|
    | blue|banana|  2| 20|
    |  red|banana|  1| 10|
    |  red|carrot|  5| 50|
    |  red|carrot|  3| 30|
    | blue| grape|  4| 40|
    +-----+------+---+---+
    



```python
!ls
```

    bar.parquet  images		      lecture_023_data.md	 zoo.orc
    data	     lecture_021_intro.ipynb  lecture_024_pyspark.ipynb
    foo.csv      lecture_022_paral.md     lecture_025_koalas.md


### Trabajando con SQL

DataFrame y Spark SQL comparten el mismo motor de ejecución, por lo que se pueden usar indistintamente sin problemas. Por ejemplo, puede registrar el DataFrame como una tabla y ejecutar un SQL fácilmente como se muestra a continuación:


```python
df.createOrReplaceTempView("tableA")
spark.sql("SELECT count(*) from tableA").show()
```

    +--------+
    |count(1)|
    +--------+
    |       8|
    +--------+
    


Además, las UDF se pueden registrar e invocar en SQL de forma inmediata:


```python
@pandas_udf("integer")
def add_one(s: pd.Series) -> pd.Series:
    return s + 1

spark.udf.register("add_one", add_one)
spark.sql("SELECT add_one(v1) FROM tableA").show()
```

    +-----------+
    |add_one(v1)|
    +-----------+
    |          2|
    |          3|
    |          4|
    |          5|
    |          6|
    |          7|
    |          8|
    |          9|
    +-----------+
    


Estas expresiones SQL se pueden mezclar y usar directamente como columnas de PySpark.


```python
from pyspark.sql.functions import expr

df.selectExpr('add_one(v1)').show()
df.select(expr('count(*)') > 0).show()
```

    +-----------+
    |add_one(v1)|
    +-----------+
    |          2|
    |          3|
    |          4|
    |          5|
    |          6|
    |          7|
    |          8|
    |          9|
    +-----------+
    
    +--------------+
    |(count(1) > 0)|
    +--------------+
    |          true|
    +--------------+
    

